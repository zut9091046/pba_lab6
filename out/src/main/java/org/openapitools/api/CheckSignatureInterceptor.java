package org.openapitools.api;

import org.openapitools.api.exceptions.WrongSignature;
import org.springframework.util.StreamUtils;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.util.ContentCachingRequestWrapper;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.Enumeration;

public class CheckSignatureInterceptor implements HandlerInterceptor {

    private static final String SECRET_KEY = "123456";
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
            throws Exception {
        // Log request information before it reaches the controller
        System.out.println("Request received. URL: " + request.getRequestURI());

        // Continue processing
        String requestBody = getRequestBody(request);
        String requestSignature = request.getHeader("x-hmac-signature");

        System.out.println(requestBody);
        System.out.println(requestSignature);

        if (requestSignature != null && HmacSignatureUtil.verifyHmacSignature(SECRET_KEY, requestBody, requestSignature)) {
            return true;
        } else {
            throw new WrongSignature();
        }
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
                           ModelAndView modelAndView) throws Exception {
        // Log response information if needed
        System.out.println("Response sent.");
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
            throws Exception {
        // Cleanup code if needed
    }

    private String getRequestBody(HttpServletRequest request) {
        try (InputStream inputStream = request.getInputStream()) {
            byte[] requestBodyBytes = StreamUtils.copyToByteArray(inputStream);
            return new String(requestBodyBytes, StandardCharsets.UTF_8);
        } catch (Exception e) {
            throw new RuntimeException("Error reading request body", e);
        }
    }
}
