package org.openapitools.api;

import org.openapitools.api.exceptions.UserAlreadyExists;
import org.openapitools.api.exceptions.UserNotFound;
import org.openapitools.api.exceptions.WrongSignature;
import org.openapitools.model.ResponseHeader;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import org.openapitools.model.Error;

import java.time.OffsetDateTime;
import java.util.UUID;

@ControllerAdvice
public class ExceptionMapper {

    @ExceptionHandler(UserAlreadyExists.class)
    public ResponseEntity<Error> handleAlreadyExistsException() {
        return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body(new Error()
                .responseHeader(new ResponseHeader()
                        .requestId(UUID.randomUUID())
                        .sendDate(OffsetDateTime.now()))
                .code("USER_ALREADY_EXISTS")
                .message("User of given ID already exists"));
    }

    @ExceptionHandler(UserNotFound.class)
    public ResponseEntity<Error> handleNotFoundException() {
        return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body(new Error()
                .responseHeader(new ResponseHeader()
                        .requestId(UUID.randomUUID())
                        .sendDate(OffsetDateTime.now()))
                .code("NOT_FOUND")
                .message("User of given ID does not exist"));
    }

    @ExceptionHandler(WrongSignature.class)
    public ResponseEntity<Error> handleWrongSignatureException() {
        return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body(new Error()
                .responseHeader(new ResponseHeader()
                        .requestId(UUID.randomUUID())
                        .sendDate(OffsetDateTime.now()))
                .code("INVALID_SIGNATURE")
                .message("Request could not be processed because of an incorrect signature"));
    }
}