package org.openapitools.api;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Base64;

import org.springframework.security.crypto.codec.Hex;
public class HmacSignatureUtil {
    public static String generateHmacSignature(String secretKey, String message) {
        try {
            Mac sha256Hmac = Mac.getInstance("HmacSHA256");
            SecretKeySpec secretKeySpec = new SecretKeySpec(secretKey.getBytes(), "HmacSHA256");
            sha256Hmac.init(secretKeySpec);

            String normalized = message.replaceAll("(\r\n|\r|\n)", "\n");

            //System.out.println(Hex.encode(normalized.getBytes()));

            byte[] hashBytes = sha256Hmac.doFinal(normalized.getBytes());
            return Base64.getEncoder().encodeToString(hashBytes);
        } catch (NoSuchAlgorithmException | InvalidKeyException e) {
            throw new RuntimeException("Error generating HMAC signature", e);
        }
    }

    public static boolean verifyHmacSignature(String secretKey, String message, String receivedSignature) {
        String calculatedSignature = generateHmacSignature(secretKey, message);
        System.out.println(calculatedSignature);
        return calculatedSignature.equals(receivedSignature);
    }
}
