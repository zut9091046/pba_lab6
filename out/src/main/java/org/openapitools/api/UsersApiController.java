package org.openapitools.api;

import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import org.openapitools.api.exceptions.UserAlreadyExists;
import org.openapitools.api.exceptions.UserNotFound;
import org.openapitools.model.*;
import org.openapitools.model.ResponseHeader;

import java.time.OffsetDateTime;
import java.util.*;


import org.openapitools.model.Error;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.util.ContentCachingRequestWrapper;

import javax.validation.constraints.*;
import javax.validation.Valid;

import javax.annotation.Generated;

@Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2024-01-09T23:39:23.366474+01:00[Europe/Warsaw]")
@Controller
@RequestMapping("${openapi.usersCRUDInterface.base-path:/api}")
public class UsersApiController implements UsersApi {

    private final NativeWebRequest request;

    @Autowired
    public UsersApiController(NativeWebRequest request) {
        this.request = request;
    }

    @Override
    public Optional<NativeWebRequest> getRequest() {
        return Optional.ofNullable(request);
    }

    private List<User> userList = new ArrayList<>();
    @Override
    public ResponseEntity<UserResponse> createUser(CreateRequest body) {

        UserResponse response = new UserResponse();

        if (userList.stream().anyMatch(user -> user.getId().equals(body.getUser().getId()))) {
            throw new UserAlreadyExists();
        }

        userList.add(body.getUser());

        response.setResponseHeader(new RequestHeader().requestId(UUID.randomUUID()).sendDate(OffsetDateTime.now()));
        response.setUser(body.getUser());
        return ResponseEntity.ok(response);
    }

    @Override
    public ResponseEntity<Void> deleteUser(UUID id) {
        boolean removed = userList.removeIf(user -> user.getId().equals(id));

        if (removed) {
            return ResponseEntity.ok().build();
        }
        else {
           throw new UserNotFound();
        }
    }

    @Override
    public ResponseEntity<UserListResponse> getAllUsers() {

        UserListResponse response = new UserListResponse();
        response.setResponseHeader(new RequestHeader().requestId(UUID.randomUUID()).sendDate(OffsetDateTime.now()));
        response.setUsersList(userList);
        return ResponseEntity
                .ok(response);
    }

    @Override
    public ResponseEntity<UserResponse> getUserById(UUID id) {
        if (!userList.stream().anyMatch(user -> user.getId().equals(id))) {
            throw new UserNotFound();
        }
        else {
            UserResponse response = new UserResponse();
            User requestedUser = userList.stream().filter(user -> user.getId().equals(id)).findFirst().get();

            response.setResponseHeader(new RequestHeader().requestId(UUID.randomUUID()).sendDate(OffsetDateTime.now()));
            response.setUser(requestedUser);
            return ResponseEntity.ok(response);
        }
    }

    @Override
    public ResponseEntity<UserResponse> updateUser(UUID id, UpdateRequest body) {
        if (!userList.stream().anyMatch(user -> user.getId().equals(id))) {
            throw new UserNotFound();
        }
        else {
            User requestedUser = userList.stream().filter(user -> user.getId().equals(id)).findFirst().get();
            requestedUser.setAge(body.getUser().getAge());
            requestedUser.setCitizenship(body.getUser().getCitizenship());
            requestedUser.setEmail(body.getUser().getEmail());
            requestedUser.setName(body.getUser().getName());
            requestedUser.setSurname(body.getUser().getSurname());
            requestedUser.setPersonalId(body.getUser().getPersonalId());

            UserResponse response = new UserResponse();
            response.setResponseHeader(new RequestHeader().requestId(UUID.randomUUID()).sendDate(OffsetDateTime.now()));
            response.setUser(requestedUser);
            return ResponseEntity.ok(response);
        }
    }
}
