package org.openapitools.model;

import java.net.URI;
import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import java.time.OffsetDateTime;
import java.util.UUID;
import org.springframework.format.annotation.DateTimeFormat;
import org.openapitools.jackson.nullable.JsonNullable;
import java.time.OffsetDateTime;
import javax.validation.Valid;
import javax.validation.constraints.*;
import io.swagger.v3.oas.annotations.media.Schema;


import java.util.*;
import javax.annotation.Generated;

/**
 * RequestHeader
 */

@Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2024-01-09T23:39:23.366474+01:00[Europe/Warsaw]")
public class RequestHeader {

  private UUID requestId;

  @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
  private OffsetDateTime sendDate;

  public RequestHeader() {
    super();
  }

  /**
   * Constructor with only required parameters
   */
  public RequestHeader(UUID requestId, OffsetDateTime sendDate) {
    this.requestId = requestId;
    this.sendDate = sendDate;
  }

  public RequestHeader requestId(UUID requestId) {
    this.requestId = requestId;
    return this;
  }

  /**
   * Get requestId
   * @return requestId
  */
  @NotNull @Valid
  @Schema(name = "requestId", requiredMode = Schema.RequiredMode.REQUIRED)
  @JsonProperty("requestId")
  public UUID getRequestId() {
    return requestId;
  }

  public void setRequestId(UUID requestId) {
    this.requestId = requestId;
  }

  public RequestHeader sendDate(OffsetDateTime sendDate) {
    this.sendDate = sendDate;
    return this;
  }

  /**
   * Date format according to ISO_8601 for example: yyyy-MM-dd'T'HH:mm:ss.SSSZ
   * @return sendDate
  */
  @NotNull @Valid
  @Schema(name = "sendDate", description = "Date format according to ISO_8601 for example: yyyy-MM-dd'T'HH:mm:ss.SSSZ", requiredMode = Schema.RequiredMode.REQUIRED)
  @JsonProperty("sendDate")
  public OffsetDateTime getSendDate() {
    return sendDate;
  }

  public void setSendDate(OffsetDateTime sendDate) {
    this.sendDate = sendDate;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    RequestHeader requestHeader = (RequestHeader) o;
    return Objects.equals(this.requestId, requestHeader.requestId) &&
        Objects.equals(this.sendDate, requestHeader.sendDate);
  }

  @Override
  public int hashCode() {
    return Objects.hash(requestId, sendDate);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class RequestHeader {\n");
    sb.append("    requestId: ").append(toIndentedString(requestId)).append("\n");
    sb.append("    sendDate: ").append(toIndentedString(sendDate)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}






//@Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2024-01-13T00:03:26.038208400+01:00[Europe/Warsaw]")
//public class RequestHeader {
//
//  private UUID requestId;
//
//  @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
//  private OffsetDateTime sendDate;
//
//  private byte[] xHMACSignature;
//
//  public RequestHeader() {
//    super();
//  }
//
//  /**
//   * Constructor with only required parameters
//   */
//  public RequestHeader(UUID requestId, OffsetDateTime sendDate, byte[] xHMACSignature) {
//    this.requestId = requestId;
//    this.sendDate = sendDate;
//    this.xHMACSignature = xHMACSignature;
//  }
//
//  public RequestHeader requestId(UUID requestId) {
//    this.requestId = requestId;
//    return this;
//  }
//
//  /**
//   * Get requestId
//   * @return requestId
//   */
//  @NotNull @Valid
//  @Schema(name = "requestId", requiredMode = Schema.RequiredMode.REQUIRED)
//  @JsonProperty("requestId")
//  public UUID getRequestId() {
//    return requestId;
//  }
//
//  public void setRequestId(UUID requestId) {
//    this.requestId = requestId;
//  }
//
//  public RequestHeader sendDate(OffsetDateTime sendDate) {
//    this.sendDate = sendDate;
//    return this;
//  }
//
//  /**
//   * Date format according to ISO_8601 for example: yyyy-MM-dd'T'HH:mm:ss.SSSZ
//   * @return sendDate
//   */
//  @NotNull @Valid
//  @Schema(name = "sendDate", description = "Date format according to ISO_8601 for example: yyyy-MM-dd'T'HH:mm:ss.SSSZ", requiredMode = Schema.RequiredMode.REQUIRED)
//  @JsonProperty("sendDate")
//  public OffsetDateTime getSendDate() {
//    return sendDate;
//  }
//
//  public void setSendDate(OffsetDateTime sendDate) {
//    this.sendDate = sendDate;
//  }
//
//  public RequestHeader xHMACSignature(byte[] xHMACSignature) {
//    this.xHMACSignature = xHMACSignature;
//    return this;
//  }
//
//  /**
//   * X-HMAC signature of request body in Base64 format
//   * @return xHMACSignature
//   */
//  @NotNull
//  @Schema(name = "X-HMAC-Signature", example = "f0jADrIur8rVdJ/yFztA8d3uil9gOJKK69hbCCE3H8Y=", description = "X-HMAC signature of request body in Base64 format", requiredMode = Schema.RequiredMode.REQUIRED)
//  @JsonProperty("X-HMAC-Signature")
//  public byte[] getxHMACSignature() {
//    return xHMACSignature;
//  }
//
//  public void setxHMACSignature(byte[] xHMACSignature) {
//    this.xHMACSignature = xHMACSignature;
//  }
//
//  @Override
//  public boolean equals(Object o) {
//    if (this == o) {
//      return true;
//    }
//    if (o == null || getClass() != o.getClass()) {
//      return false;
//    }
//    RequestHeader requestHeader = (RequestHeader) o;
//    return Objects.equals(this.requestId, requestHeader.requestId) &&
//            Objects.equals(this.sendDate, requestHeader.sendDate) &&
//            Arrays.equals(this.xHMACSignature, requestHeader.xHMACSignature);
//  }
//
//  @Override
//  public int hashCode() {
//    return Objects.hash(requestId, sendDate, Arrays.hashCode(xHMACSignature));
//  }
//
//  @Override
//  public String toString() {
//    StringBuilder sb = new StringBuilder();
//    sb.append("class RequestHeader {\n");
//    sb.append("    requestId: ").append(toIndentedString(requestId)).append("\n");
//    sb.append("    sendDate: ").append(toIndentedString(sendDate)).append("\n");
//    sb.append("    xHMACSignature: ").append(toIndentedString(xHMACSignature)).append("\n");
//    sb.append("}");
//    return sb.toString();
//  }
//
//  /**
//   * Convert the given object to string with each line indented by 4 spaces
//   * (except the first line).
//   */
//  private String toIndentedString(Object o) {
//    if (o == null) {
//      return "null";
//    }
//    return o.toString().replace("\n", "\n    ");
//  }
//}